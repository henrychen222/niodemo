/**
 * 11/12/23 night
 * 管道Pipe 结合选择器Selector开发高性能聊天室
 *
 * 范例6-13 NIO聊天室
 *
 * running reference: (允许同一个程序启动多个控制台窗口)
 * (allow running in parallel 新版 allow running multiple instances)
 * https://blog.csdn.net/weixin_43769946/article/details/103656333
 * https://blog.csdn.net/select_myname/article/details/126142974
 * https://blog.csdn.net/qq_34111745/article/details/116235143
 */
package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class ChatClient {

    public static void main(String[] args) {
        try {
            SocketChannel channel = SocketChannel.open();
            channel.configureBlocking(false); // 切换到非阻塞模式
            Selector selector = Selector.open();

            // 在客户端的选择器上, 注册一个通道, 并标识该通道所感兴趣的事件是向服务端发送连接(连接就绪). 对应于服务端的OP_ACCEPT事件
            channel.register(selector, SelectionKey.OP_CONNECT);

            int[] ports = {7777, 8888, 9999}; // 随机连接到服务端提供的一个端口上
            int port = ports[(int) (Math.random() * 3)];
            channel.connect(new InetSocketAddress("127.0.0.1", port));

            while (true) {
                selector.select();
                Set<SelectionKey> selectionKeys = selector.selectedKeys(); // selectionKeys 包含了所有通道与选择器之间的关系(请求连接, 读, 写)
                Iterator<SelectionKey> keyIterator = selectionKeys.iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey selectionKey = keyIterator.next();
                    if (selectionKey.isConnectable()) { // 判断是否连接成功
                        ByteBuffer sendBuffer = ByteBuffer.allocate(1024);
                        SocketChannel client = (SocketChannel) selectionKey.channel(); // 创建一个用于和服务端交互的channel
                        if (client.isConnectionPending()) { // 如果状态是正在连接中...
                            boolean isConnected = client.finishConnect();
                            if (isConnected) {
                                System.out.println("连接成功! 访问的端口是: " + port);
                                sendBuffer.put("connecting".getBytes()); // 向服务端发送一条测试消息
                                sendBuffer.flip();
                                client.write(sendBuffer);
                            }

                            // 在聊天室中, 对于客户端而言, 可以随时向服务端发送消息(写操作), 因此, 需要建立一个单独写线程
                            new Thread(() -> {
                                while (true) {
                                    try {
                                        sendBuffer.clear();
                                        InputStreamReader reader = new InputStreamReader(System.in); // 接收用户从控制台输入的内容, 并发送给服务端
                                        BufferedReader bReader = new BufferedReader(reader);
                                        String message = bReader.readLine();
                                        sendBuffer.put(message.getBytes());
                                        sendBuffer.flip();
                                        client.write(sendBuffer);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                        client.register(selector, SelectionKey.OP_READ); // 标记通道感兴趣的事件是读取服务端消息(读就绪)
                    } else if (selectionKey.isReadable()) { // 客户端读取服务端的反馈消息
                        SocketChannel client = (SocketChannel) selectionKey.channel();
                        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
                        int len = client.read(readBuffer); // 将服务端的反馈消息放入readBuffer中
                        if (len > 0) {
                            String receive = new String(readBuffer.array(), 0, len);
                            System.out.println(receive);
                        }
                    }
                }
                selectionKeys.clear();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
