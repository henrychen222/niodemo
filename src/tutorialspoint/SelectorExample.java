/**
 * 11/02/21 morning
 * https://www.tutorialspoint.com/java_nio/java_nio_selector.htm
 */

package tutorialspoint;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

public class SelectorExample {

    public static void main(String[] args) throws IOException {
        String demo_text = "This is a demo String";
        Selector selector = Selector.open();
        ServerSocketChannel serverSocket = ServerSocketChannel.open();
        serverSocket.bind(new InetSocketAddress("localhost", 5454));
        serverSocket.configureBlocking(false);
        serverSocket.register(selector, SelectionKey.OP_ACCEPT);
        ByteBuffer buffer = ByteBuffer.allocate(256);
        tr("serverSocket", serverSocket);
        tr("buffer", buffer);
        while (true) {
            selector.select();
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iter = selectedKeys.iterator();
            while (iter.hasNext()) {
                SelectionKey key = iter.next();
                tr("key", key);
                int interestOps = key.interestOps();
                tr(interestOps);
                if (key.isAcceptable()) {
                    tr("Acceptable");
                    SocketChannel client = serverSocket.accept();
                    client.configureBlocking(false);
                    client.register(selector, SelectionKey.OP_READ);
                }
                if (key.isReadable()) {
                    tr("isReadable");
                    SocketChannel client = (SocketChannel) key.channel();
                    client.read(buffer);
                    if (new String(buffer.array()).trim().equals(demo_text)) {
                        client.close();
                        tr("Not accepting client messages anymore");
                    }
                    buffer.flip();
                    client.write(buffer);
                    buffer.clear();
                }
                iter.remove();
            }
        }
    }

    static void tr(Object... o) {
        System.out.println(Arrays.deepToString(o));
    }
}
