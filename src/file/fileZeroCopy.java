/**
 * 11/13/23 afternoon
 *
 * 范例6-8 零拷贝
 * 使用零拷贝的方式, 完成文件的复制
 *
 * how to test:
 * create abc.txt with any content in path, run the code
 *
 * expect result: abc1.txt will generate, same content with abc.txt
 */
package file;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static util.Constant.*;

public class fileZeroCopy {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        FileChannel inChannel = FileChannel.open(Paths.get(FILE_ABC_LOCATION), StandardOpenOption.READ);
        FileChannel outChannel = FileChannel.open(Paths.get(FILE_ABC1_LOCATION), StandardOpenOption.WRITE,
                StandardOpenOption.READ, StandardOpenOption.CREATE);
        inChannel.transferTo(0, inChannel.size(), outChannel);
        outChannel.transferFrom(inChannel, 0, inChannel.size());
        inChannel.close();
        outChannel.close();
        long end = System.currentTimeMillis();
        System.out.println("复制操作消耗的时间(毫秒): " + (end - start));
    }
}
