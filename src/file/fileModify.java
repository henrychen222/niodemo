/**
 * 11/13/23 afternoon
 *
 * 范例6-7 文件修改
 * 在直接缓冲区中 使用MappedByteBuffer修改文件的内容
 *
 * how to test:
 * create abc.txt with "Helloworld" content in path, run the code
 *
 * expected result: HXlYoworld
 */
package file;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import static util.Constant.*;

public class fileModify {
    public static void main(String[] args) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(FILE_ABC_LOCATION, "rw");
        FileChannel fileChannel = raf.getChannel();
        MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, raf.length());
        mappedByteBuffer.put(1, (byte) 'X');
        mappedByteBuffer.put(3, (byte) 'Y');
        raf.close();
    }
}
