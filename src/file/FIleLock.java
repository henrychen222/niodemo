/**
 * 11/15/23 afternoon
 *
 * 范例6-11 资源加锁
 * 给某一个文件加锁, 防止并发访问时引起的数据不安全
 */
package file;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import static util.Constant.*;

public class FIleLock {
    public static void main(String[] args) throws IOException, InterruptedException {
        RandomAccessFile raf = new RandomAccessFile(FILE_ABC_LOCATION, "rw");
        FileChannel fileChannel = raf.getChannel();

        /**
         将abc.txt中position=2, size=4的内容加锁(即只对文件的部分内容加了锁)
         lock()第3个布尔参数的含义如下
         true: 共享锁. 实际上是指"读共享". 某一线程将资源锁住之后, 其他线程只能读, 不能写该资源
         false: 独占锁. 某一线程将资源锁住之后, 其他线程既不能读又不能写该资源
         */
        FileLock fileLock = fileChannel.lock(2, 4, true);
        System.out.println("main线程将abc.txt锁3秒");
        new Thread(() -> {
            try {
                byte[] bs = new byte[8];
                raf.read(bs, 0, 8); // 新线程对abc.txt进行读操作
                raf.write("ccccccccc".getBytes(), 0, 8); // 新线程对abc.txt进行写操作
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }).start();
        Thread.sleep(3000); // 模拟main线程将abc.txt锁3秒的操作
        System.out.println("3秒结束, main释放锁");
        fileLock.release();
    }
}
