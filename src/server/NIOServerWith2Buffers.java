/**
 * 11/13/23 afternoon
 * 管道Pipe 结合选择器Selector开发高性能聊天室 II
 *
 * 范例6-14 多缓冲区操作.
 * 服务端通过两个缓冲区接收客户端传来的消息 (借用ChatClient作为客户端)
 */
package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class NIOServerWith2Buffers {

    public static void main(String[] args) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        ServerSocket serverSocket = serverSocketChannel.socket();
        int port = 8888;
        serverSocket.bind(new InetSocketAddress(port));
        System.out.println("服务端启动成功, 端口 " + port);
        ByteBuffer[] buffers = new ByteBuffer[2];
        buffers[0] = ByteBuffer.allocate(4);
        buffers[1] = ByteBuffer.allocate(8);
        int bufferSum = 4 + 8;
        SocketChannel socketChannel = serverSocketChannel.accept();
        while (true) {
            long totalReadBytes = 0;
            while (totalReadBytes < bufferSum) {
                long eachReadBytes = socketChannel.read(buffers);
                totalReadBytes += eachReadBytes;
                System.out.println("读取到的数据大小: " + eachReadBytes);
            }
            // 如果buffers已满
            for (ByteBuffer buffer : buffers) {
                buffer.flip();
            }
        }
    }
}
