/**
 * 11/11/23 night
 * 管道Pipe 结合选择器Selector开发高性能聊天室
 *
 * 范例6-13 NIO聊天室
 */
package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class ChatServer {
    /**
     * clientsMap: 保存所有的客户端
     * key: 客户端的名字
     * value: 客户端连接服务器的channel
     */
    private static Map<String, SocketChannel> clientsMap = new HashMap<>();

    public static void main(String[] args) throws Exception {
        int[] ports = {7777, 8888, 9999};
        Selector selector = Selector.open();
        for (int port : ports) {
            ServerSocketChannel channel = ServerSocketChannel.open();
            channel.configureBlocking(false);

            // 将聊天服务绑定到7777, 8888, 9999三个端口上
            ServerSocket serverSocket = channel.socket();
            serverSocket.bind(new InetSocketAddress(port));
            System.out.println("服务端启动成功, 端口 " + port);

            // 在服务器端的选择器上, 注册一个通道, 并标识该通道所感兴趣的事件是接收客户端连接(接收就绪)
            channel.register(selector, SelectionKey.OP_ACCEPT);
        }

        while (true) { // 一直阻塞, 直到选择器上存在已经就绪的通道(包含感兴趣的事件)
            selector.select();
            Set<SelectionKey> selectionKeys = selector.selectedKeys(); // selectionKeys: 包含了所有通道与选择器之间的关系(接收连接, 读, 写)
            Iterator<SelectionKey> keyIterator = selectionKeys.iterator();

            while (keyIterator.hasNext()) { // 如果selector中有多个就绪通道(接收就绪, 读就绪, 写就绪等), 则遍历这些通道
                SelectionKey selectionKey = keyIterator.next();
                String receive = null;
                SocketChannel clientChannel; // 与客户端交互的通道
                try {
                    if (selectionKey.isAcceptable()) { // 接收就绪(已经可以接收客户端的连接了)
                        ServerSocketChannel server = (ServerSocketChannel) selectionKey.channel();
                        clientChannel = server.accept();
                        clientChannel.configureBlocking(false); // 切换到非阻塞模式

                        // 再在服务端的选择器上, 注册第二个通道, 并标识该通道所感兴趣的事件是接收客户端发来的消息(读就绪)
                        clientChannel.register(selector, SelectionKey.OP_READ);

                        String key = "key" + (int) (Math.random() * 9000 + 10000); // 用"key四位随机数"的形式模拟客户端的key值
                        clientsMap.put(key, clientChannel); // 将该建立完毕连接的通道保存到clientsMap中

                    } else if (selectionKey.isReadable()) { // 读就绪(已经可以读取客户端发来的消息了)
                        clientChannel = (SocketChannel) selectionKey.channel();
                        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
                        int result = -1;
                        try {
                            result = clientChannel.read(readBuffer); // 将服务端读取到的客户端消息, 放入readBuffer中
                        } catch (IOException e) { // 如果终止客户端, 则read()会抛出IOException异常, 可以依次判断是否有客户端退出
                            String clientKey = getClientKey(clientChannel); // 获取退出连接的client对应的key
                            System.out.println(" 客户端: " + clientKey + " 退出聊天室");
                            clientsMap.remove(clientKey);
                            clientChannel.close();
                            selectionKey.cancel();
                            continue;
                        }
                        if (result > 0) {
                            readBuffer.flip();
                            Charset charset = StandardCharsets.UTF_8;
                            receive = String.valueOf(charset.decode(readBuffer).array());
                            System.out.println(clientChannel + " : " + receive);
                            if ("connecting".equals(receive)) { // 处理客户端第一次发来的连接测试信息
                                receive = "新客户端加入聊天";
                            }
                            selectionKey.attach(receive); // 将读取到的客户端消息保存在attachment中, 用于后续向所有客户端转发此消息
                            selectionKey.interestOps(SelectionKey.OP_WRITE); // 将通道所感兴趣的事件标识为: 向客户端发送消息(写就绪)
                        }
                    } else if (selectionKey.isWritable()) { // 写就绪
                        clientChannel = (SocketChannel) selectionKey.channel();
                        String sendKey = getClientKey(clientChannel); // 获取发送消息从client对应的key

                        for (String key : clientsMap.keySet()) {  // 将接收到的消息广播给所有的client
                            SocketChannel eachClient = clientsMap.get(key);
                            ByteBuffer broadcastMsg = ByteBuffer.allocate(1024);
                            broadcastMsg.put((sendKey + ": " + selectionKey.attachment()).getBytes());
                            broadcastMsg.flip();
                            eachClient.write(broadcastMsg);
                        }

                        selectionKey.interestOps(SelectionKey.OP_READ);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            selectionKeys.clear();
        }
    }

    public static String getClientKey(SocketChannel channel) {
        for (String key : clientsMap.keySet()) {
            if (channel == clientsMap.get(key)) return key;
        }
        return null;
    }
}
