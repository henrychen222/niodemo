/**
 * 11/15/23 night
 *
 * 范例6-9 NIO文件传输
 * 使用NIO实现客户端向服务端发送文件的功能
 */
package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static util.Constant.*;

public class FileTransferServer {
    public static void main(String[] args) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        FileChannel outChannel = FileChannel.open(Paths.get(FILE_ABC_LOCATION),
                StandardOpenOption.WRITE, StandardOpenOption.CREATE);

        serverSocketChannel.bind(new InetSocketAddress(8888));
        SocketChannel channel = serverSocketChannel.accept();
        System.out.println("连接成功");
        long start = System.currentTimeMillis();
        ByteBuffer buffer = ByteBuffer.allocate(1024);

        while (channel.read(buffer) != -1) {
            buffer.flip();
            outChannel.write(buffer);
            buffer.clear();
        }
        System.out.println("接收成功");
        channel.close();
        outChannel.close();
        serverSocketChannel.close();
        long end = System.currentTimeMillis();
        System.out.println("服务端接收文件耗时: " + (end - start));
    }
}
